import Vue from 'vue';
import App from '@/App.vue';
import router from './router';
import Model from "@/models/model";

Vue.config.productionTip = false;

Vue.filter('capitalize', function (value: string): string {
  if (!value) return '';
  value = value.toString();
  return value.charAt(0).toUpperCase() + value.slice(1);
})

export const apiUrl = 'http://employees-back.test/api';

export const byId = (a: Model, b: Model): number => a.id - b.id;

new Vue({
  router,
  render: h => h(App)
}).$mount('#app');
