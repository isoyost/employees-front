import axios, {AxiosResponse} from 'axios';
import Job from '@/models/job';
import {apiUrl} from '@/main';

export default class JobService {
    private static resource = 'jobs';

    static async asyncGetAll(): Promise<Job[]> {
        const { data } = await axios.get(this.getUrl());
        return data.map((datum: any): Job => new Job(datum.id, datum.title, datum.minimal_salary));
    }

    static async asyncGet(id: number): Promise<Job> {
        const { data } = await axios.get(this.getUrl(id));
        return new Job(data.id, data.title, data.minimal_salary);
    }

    static asyncUpdate(id: number, job: Job): Promise<AxiosResponse> {
        return axios.put(this.getUrl(id), job);
    }

    static async asyncStore(job: Job): Promise<Job> {
        const { data } = await axios.post(this.getUrl(), job);
        return new Job(data.id, data.title, data.minimal_salary);
    }

    static asyncDelete(id: number): Promise<AxiosResponse> {
        return axios.delete(this.getUrl(id));
    }

    private static getUrl(id: number | undefined = undefined): string {
        let url = `${apiUrl}/${this.resource}`;
        if (id !== undefined) url += `/${id}`;
        return url;
    }
}
