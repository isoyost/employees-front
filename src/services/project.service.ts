import axios, {AxiosResponse} from 'axios';
import Project from '@/models/project';
import {apiUrl} from '@/main';

export default class ProjectService {
    private static resource = 'projects';

    static async asyncGetAll(): Promise<Project[]> {
        const { data } = await axios.get(this.getUrl());
        return data.map((datum: any): Project => new Project(datum.id, datum.title, datum.value));
    }

    static async asyncGet(id: number): Promise<Project> {
        const { data } = await axios.get(this.getUrl(id));
        return new Project(data.id, data.title, data.value);
    }

    static asyncUpdate(id: number, project: Project): Promise<AxiosResponse> {
        return axios.put(this.getUrl(id), project);
    }

    static async asyncStore(project: Project): Promise<Project> {
        const { data } = await axios.post(this.getUrl(), project);
        return new Project(data.id, data.title, data.value);
    }

    static asyncDelete(id: number): Promise<AxiosResponse> {
        return axios.delete(this.getUrl(id));
    }

    private static getUrl(id: number | undefined = undefined): string {
        let url = `${apiUrl}/${this.resource}`;
        if (id !== undefined) url += `/${id}`;
        return url;
    }
}
