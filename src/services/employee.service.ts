import axios, {AxiosResponse} from 'axios';
import Employee from '@/models/employee';
import {apiUrl} from '@/main';

export default class EmployeeService {
    private static resource = 'employees';

    static async asyncGetAll(): Promise<Employee[]> {
        const { data } = await axios.get(this.getUrl());
        return data.map((datum: any): Employee => new Employee(datum.id, datum.name, datum.email, datum.salary));
    }

    static async asyncGet(id: number): Promise<Employee> {
        const { data } = await axios.get(this.getUrl(id));
        return new Employee(data.id, data.name, data.email, data.salary);
    }

    static asyncUpdate(id: number, employee: Employee): Promise<AxiosResponse> {
        return axios.put(this.getUrl(id), employee);
    }

    static async asyncStore(employee: Employee): Promise<Employee> {
        const { data } = await axios.post(this.getUrl(), employee);
        return new Employee(data.id, data.name, data.email, data.salary);
    }

    static asyncDelete(id: number): Promise<AxiosResponse> {
        return axios.delete(this.getUrl(id));
    }

    private static getUrl(id: number | undefined = undefined): string {
        let url = `${apiUrl}/${this.resource}`;
        if (id !== undefined) url += `/${id}`;
        return url;
    }
}
