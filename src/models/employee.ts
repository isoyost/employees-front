import Model from '@/models/model';

export default class Job extends Model{
    constructor(id = 0, name = '', email = '', salary = 0) {
        super(id);
        this.name = name;
        this.email = email;
        this.salary = salary;
    }

    name: string;
    email: string;
    salary: number;
}
