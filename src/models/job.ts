import Model from '@/models/model';

export default class Job extends Model{
    constructor(id = 0, title = '', minimal_salary = 0) {
        super(id);
        this.title = title;
        this.minimal_salary = minimal_salary;
    }

    title: string;
    minimal_salary: number;
}
