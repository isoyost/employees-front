import Model from '@/models/model';

export default class Job extends Model{
    constructor(id = 0, title = '', value = 0) {
        super(id);
        this.title = title;
        this.value = value;
    }

    title: string;
    value: number;
}
