import Vue from 'vue';
import VueRouter from 'vue-router';
import Employees from '@/components/views/Employees/Employees.vue';
import Jobs from '@/components/views/Jobs/Jobs.vue';
import Projects from '@/components/views/Projects/Projects.vue';
import EmployeeCreate from '@/components/views/Employees/EmployeeCreate.vue';
import EmployeeEdit from '@/components/views/Employees/EmployeeEdit.vue';
import JobCreate from '@/components/views/Jobs/JobCreate.vue';
import JobEdit from '@/components/views/Jobs/JobEdit.vue';
import ProjectCreate from '@/components/views/Projects/ProjectCreate.vue';
import ProjectEdit from '@/components/views/Projects/ProjectEdit.vue';
import {RouteConfig} from "vue-router/types/router";

Vue.use(VueRouter);

export const NavbarRoutes: RouteConfig[] = [
    {
        path: '/employees',
        name: 'employees',
        component: Employees,
    },
    {
        path: '/jobs',
        name: 'jobs',
        component: Jobs,
    },
    {
        path: '/projects',
        name: 'projects',
        component: Projects,
    },
];

const OtherRoutes: RouteConfig[] = [
    {
        path: '/employees/create',
        name: 'employees-create',
        component: EmployeeCreate,
    },
    {
        path: '/employees/:id/edit',
        name: 'employees-edit',
        component: EmployeeEdit,
    },
    {
        path: '/jobs/create',
        name: 'jobs-create',
        component: JobCreate,
    },
    {
        path: '/jobs/:id/edit',
        name: 'jobs-edit',
        component: JobEdit,
    },
    {
        path: '/projects/create',
        name: 'projects-create',
        component: ProjectCreate,
    },
    {
        path: '/projects/:id/edit',
        name: 'projects-edit',
        component: ProjectEdit,
    },
];

export default new VueRouter({
    routes: [
        ...NavbarRoutes,
        ...OtherRoutes,
    ]
});
