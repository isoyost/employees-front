import Model from '@/models/model';

export default class DeleteModelEvent{
    constructor(model: Model, index: number) {
        this.model = model;
        this.index = index;
    }

    model: Model;
    index: number;
}
